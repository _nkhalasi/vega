package org.vega.addressbook.service

import org.specs2.mutable.Specification
import spray.testkit.Specs2RouteTest
import spray.http._
import StatusCodes._

class ContactServiceSpec extends Specification with Specs2RouteTest with ContactService {
    def actorRefFactory = system
    "ContactService" should {
        "return JSON representation of a contact for GET requests of an existing contact" in {
            Get("/contact") ~> myRoute ~> check {
                responseAs[String] must contain("hello")
            }
        }
    }
}