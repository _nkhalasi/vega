package org.vega.addressbook.service

import akka.actor.Actor
import spray.routing._
import spray.http._
import MediaTypes._

class ContactServiceActor extends Actor with ContactService {
    def actorRefFactory = context
    def receive = runRoute(myRoute)
}

trait ContactService extends HttpService {
    val myRoute =
        path("contact") {
            post {
                respondWithMediaType(`application/json`) { ctx => 
                    println(ctx.request.headers)
                    ctx complete {
                        <html>
                            <body>
                                <h1>Say hello to <i>spray-routing</i> on <i>spray-can</i>!</h1>
                            </body>
                        </html>
                    }
                }
            }
            get {
                respondWithMediaType(`application/json`) { ctx => 
                    println(ctx.request.headers)
                    ctx complete {
                        "{'greeting':'hello'}"
                    }
                }
            }
        }
}