package org.vega.addressbook.models

import scala.slick.driver.JdbcProfile
import scala.slick.jdbc.JdbcBackend.Database
import org.vega.addressbook.db.DriverComponent

case class Contact(id:Option[Long]=None, firstName:String, middleName:Option[String]=None, lastName:String, address:Option[Address]=None)

import spray.json._
import DefaultJsonProtocol._
object ContactJsonProtocol extends DefaultJsonProtocol {
	import AddressJsonProtocol._
	implicit val contactJsonFormat = jsonFormat5(Contact)
}///:~

trait ContactComponent { this: DriverComponent with AddressComponent =>
	import driver.simple._
	class Contacts(tag: Tag) extends Table[Contact](tag, "CONTACTS") {
		def id = column[Option[Long]]("ID", O.PrimaryKey, O.AutoInc)
		def firstName = column[String]("FNAME", O.DBType("VARCHAR(30)"))
		def middleName = column[Option[String]]("MNAME", O.DBType("VARCHAR(30)"), O.Nullable)
		def lastName = column[String]("LNAME", O.DBType("VARCHAR(30)"))
		def addressId = column[Option[Long]]("ADDRESS_ID", O.Nullable)
		def address = foreignKey("address_fk", addressId, addresses)(_.id, onUpdate=ForeignKeyAction.Restrict, onDelete=ForeignKeyAction.Cascade)
		def * = (id, firstName, middleName, lastName) <> (
				(c:Tuple4[Option[Long], String, Option[String], String]) => Contact(c._1, c._2, c._3, c._4),
				(c:Contact) => Some(c.id, c.firstName, c.middleName, c.lastName)
			)
	}
}///:~

class ContactDAO(val driver: JdbcProfile, val db: Database) extends ContactComponent with DriverComponent with AddressComponent {
	import driver.simple._
	val contacts = TableQuery[Contacts]
	def findContactById(id: Long) = contacts.filter(_.id  === id)
	def insert(contact: Contact)(implicit session:Session) = (contacts returning contacts.map(_.id)) += contact
}///:~