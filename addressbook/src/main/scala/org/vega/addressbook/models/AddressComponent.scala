package org.vega.addressbook.models

import org.vega.addressbook.db.DriverComponent

case class Address(houseNumber:String, building:String, city:String, state:String, zipcode:String, 
	country:String, landmark:Option[String]=None, street:Option[String]=None, id:Option[Long]=None)


import spray.json._
import DefaultJsonProtocol._
object AddressJsonProtocol extends DefaultJsonProtocol with NullOptions {
	implicit val addressJsonFormat = jsonFormat9(Address)
}

trait AddressComponent { this: DriverComponent =>
	import driver.simple._
	class Addresses(tag: Tag) extends Table[Address](tag, "ADDRESSES") {
		def id = column[Option[Long]]("ID", O.PrimaryKey, O.AutoInc)
		def houseNumber = column[String]("HOUSE_NUMBER", O.DBType("VARCHAR(10)"))
		def building = column[String]("BUILDING", O.DBType("VARCHAR(50)"))
		def landmark = column[Option[String]]("LANDMARK", O.DBType("VARCHAR(50)"), O.Nullable)
		def street = column[Option[String]]("STREET", O.DBType("VARCHAR(50)"), O.Nullable)
		def city = column[String]("CITY", O.DBType("VARCHAR(30)"))
		def state = column[String]("STATE", O.DBType("VARCHAR(30)"))
		def zipcode = column[String]("ZIPCODE", O.DBType("CHAR(8)"))
		def country = column[String]("COUNTRY", O.DBType("VARCHAR(50)"))
		def * = (houseNumber, building, city, state, zipcode, country, landmark, street, id) <> (Address.tupled, Address.unapply)
	}

	val addresses = TableQuery[Addresses]
}///:~