package org.vega.addressbook.db

import scala.slick.driver.JdbcProfile
import scala.slick.jdbc.JdbcBackend.Database
import org.vega.common.util.datasource.{DatasourceBuilder, DatasourceConfig}

/** The slice of the cake which provides the Slick driver */
trait DriverComponent {
  val driver: JdbcProfile
  val db: Database
}///:~

trait ProductionDatasource extends DatasourceBuilder with DatasourceConfig {
	override val datasourceConfig = org.vega.common.util.encryptor.EncryptablePropertiesReader("datasource.properties")
}///:~

class DBContext(val driver: JdbcProfile, val db: Database) extends DriverComponent 

object DBContext extends ProductionDatasource {
	def apply() = new DBContext(scala.slick.driver.MySQLDriver, Database.forDataSource(datasource))
}///:~