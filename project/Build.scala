import sbt._
import Keys._

object CeresBuild extends Build {
    val akkaV = "2.3.5"
    val sprayV = "1.3.1"
    lazy val defaultSettings = Defaults.defaultSettings ++ Seq (
            version := "1.0-SNAPSHOT",
            scalaVersion := "2.11.4",
            scalacOptions := Seq("-unchecked", "-deprecation", "-encoding", "utf8"),
            fork in Test := true,
            javaOptions in Test := Seq("-Dmaster.enc.key=SRLWR8GthPGBaXNqH9kahhKd"),
            libraryDependencies ++= Seq(
                    "mysql" % "mysql-connector-java" % "5.1.30",
                    "org.rogach" %% "scallop" % "0.9.5",
                    "org.jasypt" % "jasypt" % "1.9.2",
                    "com.typesafe" % "config" % "1.2.1",
                    "c3p0" % "c3p0" % "0.9.1.2",
                    "org.specs2" %% "specs2" % "2.4.2" % "test",
                    "org.scalacheck" %% "scalacheck" % "1.11.5" % "test"
                )
        )
    lazy val commonUtils = Project("common-utils", file("common-utils"),
            settings=defaultSettings
        )
    lazy val addressbook = Project("addressbook", file("addressbook"), 
            settings=defaultSettings ++ Seq(
                fork := true,
                javaOptions := Seq("-Dmaster.enc.key=SRLWR8GthPGBaXNqH9kahhKd"),
                libraryDependencies ++= Seq(
                    "com.typesafe.slick"  %% "slick" % "2.1.0",
                    "io.spray"            %%  "spray-json"    % "1.3.0",
                    "io.spray"            %%  "spray-can"     % sprayV,
                    "io.spray"            %%  "spray-routing" % sprayV,
                    "io.spray"            %%  "spray-testkit" % sprayV  % "test",
                    "com.typesafe.akka"   %%  "akka-actor"    % akkaV,
                    "com.typesafe.akka"   %%  "akka-testkit"  % akkaV   % "test"
                )
            )
        ).aggregate(commonUtils).dependsOn(commonUtils)

    import com.github.bigtoast.sbtliquibase.LiquibasePlugin._
    lazy val dbvcs = Project("dbvcs", file("dbvcs"),
            settings=defaultSettings ++ Seq(
                fork := true,
                libraryDependencies ++= Seq(
                    "org.liquibase" % "liquibase-core" % "3.3.0",
                    "org.yaml" % "snakeyaml" % "1.14"
                    )
                ) ++ liquibaseSettings ++ Seq (
                    liquibaseUsername := "vegaadmin",
                    liquibasePassword := "vegaadmin",
                    liquibaseDriver := "com.mysql.jdbc.Driver",
                    liquibaseUrl := "jdbc:mysql://localhost:3306/addressbook",
                    liquibaseChangelog := "dbvcs/src/main/resources/vega-master.xml"
                )
        )
    lazy val root = Project("vega", file("."), settings=defaultSettings).aggregate(addressbook).dependsOn(addressbook)
}///:~