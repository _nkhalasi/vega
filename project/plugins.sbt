addSbtPlugin("com.orrsella" % "sbt-sublime" % "1.0.9")

addSbtPlugin("io.spray" % "sbt-revolver" % "0.7.2")

resolvers += "bigtoast-github" at "http://bigtoast.github.com/repo/"

addSbtPlugin("com.github.bigtoast" % "sbt-liquibase" % "0.5")
