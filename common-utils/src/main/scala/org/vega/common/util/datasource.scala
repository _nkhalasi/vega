package org.vega.common.util.datasource

//REF: http://stackoverflow.com/questions/13661339/slick-how-to-write-db-agnostic-apps-and-perform-first-time-db-initialization
trait DatasourceConfig {
	val datasourceConfig: org.jasypt.properties.EncryptableProperties
	def driverClass = datasourceConfig.getProperty("datasource.driverclass")
	def jdbcUrl = datasourceConfig.getProperty("datasource.jdbcurl")
	def dbUser = datasourceConfig.getProperty("datasource.username")
	def dbPassword = datasourceConfig.getProperty("datasource.password")
	def dbHost = datasourceConfig.getProperty("datasource.dbhost")
	def dbPort = datasourceConfig.getProperty("datasource.dbport")
}///:~

trait DatasourceBuilder { this: DatasourceConfig =>
    private val c3p0cp = 
        (driverClass: String, jdbcUrl: String, dbUser: String, dbPassword: String) => {
            import com.mchange.v2.c3p0.{ComboPooledDataSource}
            val ds = new ComboPooledDataSource()
            ds.setDriverClass(driverClass)
            ds.setJdbcUrl(jdbcUrl)
            ds.setUser(dbUser)
            ds.setPassword(dbPassword)
            ds
        }
    def datasource = c3p0cp(driverClass, jdbcUrl, dbUser, dbPassword)
}///:~