package org.vega.common.util.encryptor

import org.specs2.mutable._

class EncryptorSpec extends Specification {
	"The data.properties file" should {
		val props = EncryptablePropertiesReader("data.properties")
		"throw EncryptionOperationNotPossibleException when trying to read invalid value for encrypted.property1" in {
			props.getProperty("encrypted.property1") must throwA[org.jasypt.exceptions.EncryptionOperationNotPossibleException]
		}
		"throw EncryptionOperationNotPossibleException when trying to read invalid value for encrypted.property2" in {
			props.getProperty("encrypted.property2") must throwA[org.jasypt.exceptions.EncryptionOperationNotPossibleException]
		}
		"contain encrypted.property3 with value as 'Fun World'" in {
			props.getProperty("encrypted.property3") === "Fun World"
		}
		"contain encrypted.property4 with value as four spaces" in {
			props.getProperty("encrypted.property4") === "    "
		}
		"contain plaintext.property1 with value as 'Hello World'" in {
			props.getProperty("plaintext.property1") === "Hello World"
		}
		"contain property.blank.value with blank value" in {
			props.getProperty("property.blank.value") === ""
		}
	}
}