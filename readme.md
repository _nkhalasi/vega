
###Creating the application database
```
/home/nkhalasi/> mysql -u root -p mysql

mysql> CREATE DATABASE addressbook DEFAULT CHARSET utf8 DEFAULT COLLATE utf8_general_ci;

mysql> GRANT ALL ON addressbook.* to 'vegaadmin'@'localhost' IDENTIFIED BY '<vegaadmin password>';
```

###Using `liquibase` to manage application databse via `SBT`
```
/path/to/vega/project/> sbt
> dbvcs/liquibase-drop-all
> dbvcs/liquibase-validate-changelog
> dbvcs/liquibase-status
> dbvcs/liquibase-update
```
